// const { formDataToBlob } = require("formdata-polyfill/esm.min");


let form = document.getElementById("loginId");

let firstName = document.getElementById("FirstNameId");
let lastName = document.getElementById("SecondNameId");

let firstErr = document.getElementById("firstErr");
let lastErr = document.getElementById("secondErr");

let success = document.getElementById("done");
let nameValue = document.getElementById("nameOfThePerson");

firstErr.style.color="red";
lastErr.style.color="red";

form.addEventListener("submit", (event) => {
    event.preventDefault();

    firstErr.innerText = "";
    lastErr.innerText = "";
    success.innerText = "";
    nameValue.innerText = "";

    if (!firstName.value && !lastName.value) {
        firstErr.innerText = "Missing First Name";
        lastErr.innerText = "Missing Last Name";
    } else if (!firstName.value) {
        firstErr.innerText = "Missing First Name";
    } else if (!lastName.value) {
        lastErr.innerText = "Missing Last Name";
    } else {
        success.innerText = "Form successfully submitted";
        nameValue.innerText = firstName.value + " " + lastName.value;
        firstErr.value = "";
        lastErr.value = "";
        form.reset();
    }
});



